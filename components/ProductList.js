import React from 'react'

export default class extends React.Component {
  render () {
    return <ul>
      { this.props.products.map(p =>
        <li key={p.name}>{p.name}</li>
      )}
    </ul>
  }
}
