import ProductList from '../components/ProductList'

import products from '../database/products'

export default class extends React.Component {
  static async getInitialProps() {
    // TODO this should call an API instead, that way both server and cliend side are able to.
    return {
      products: await products.list()
    }
  }

  render () {
    return <main>
      <header>
        <h1>Next.JS sandbox</h1>
        <p>Interested to see whether/how it handles conditional SSR.</p>
      </header>
      <section>
        <ProductList products={this.props.products}/>
      </section>
      <style jsx>{`
        p {
          color: gray;
        }
      `}</style>
    </main>
  }
}
