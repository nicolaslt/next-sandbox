# Next.JS Sandbox

A quick & dirty play with [Next.JS](https://nextjs.org), because I need a hands on to understand those things.

At the minute I'm a bit disappointed by the data fetching model (`getInitialProps`), that forces the entire page to have control over all its contained data. Or otherwise to write React components with `componentDidMount` -which is only ran on the client side so kinda defeats the point-. For example, on a e-commerce website, product page: the "recommended products" section should be self contained, but as I understand Next.JS so far we are either forced to make the product page itself load the recommended products and pass it all the way down (context/prop/state), or to make it a client-side only component which turns the app closer to the SPA model.

I'm hoping I'm just misunderstanding things.

To be honest I probably wouldn't have minded this if I wasn't also learning about [micro-frontend](https://micro-frontends.org/) at the same time.
