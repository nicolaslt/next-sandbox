export default {
  async list() {
    // TODO load that from fs, to make sure I understand when/how data is sent to client side
    return [
      { name: 'table'},
      { name: 'chair'},
      { name: 'office desk'}
    ]
  }
}
